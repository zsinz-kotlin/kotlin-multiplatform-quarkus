import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack

plugins {
    kotlin("multiplatform") version "1.4.0"
    kotlin("plugin.serialization") version "1.4.0"
    kotlin("kapt") version "1.4.0"

    id("io.quarkus") version "1.7.1.Final"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.4.0"
    id("com.adarshr.test-logger") version "2.0.0"
    id("io.gitlab.arturbosch.detekt") version "1.10.0"
    id("com.github.jk1.dependency-license-report") version "1.13"
}
group = "com.nanthealth"
version = "1.0-SNAPSHOT"

val quarkusPlatformGroupId: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformVersion: String by project
val quarkusPluginVersion: String by project
val kotlinVersion: String by project
val kotlinxVersion: String by project
val kotinResultVersion: String by project
val kotlinxCoroutinesTestVersion: String by project

val kotlinJsExtensionsVersion = "1.0.1-pre.111-kotlin-1.4.0" // M3?
val kotlinReactVersion = "16.13.1-pre.111-kotlin-1.4.0"
val kotlinReactDomVersion = "16.13.1-pre.111-kotlin-1.4.0"
val kotlinReactRouterDomVersion = "5.1.2-pre.111-kotlin-1.4.0"
val kotlinReduxVersion = "4.0.0-pre.111-kotlin-1.4.0"
val kotlinReactReduxVersion = "5.0.7-pre.111-kotlin-1.4.0"
val kotlinCssVersion = "1.0.0-pre.111-kotlin-1.4.0"
val ktorVersion = "1.4.0"
val kotlinSerializationVersion = "1.0.0-RC"
val kotlinReactMdwcVersion = "1.0.0-SNAPSHOTX"
val kotlinxCoroutinesVersion = "1.3.9"
val kotlinxHtmlJvmVersion = "0.7.2"
val kotlinJacksonModuleVersion = "2.11.0"

val restAssuredVersion = "4.3.1"
val assertjCoreVersion = "3.16.1"
val slf4jTestVersion = "1.0.0"

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/kotlin/ktor")
    }
    maven{
        url = uri("https://kotlin.bintray.com/kotlinx/")
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlin-js-wrappers")
    }
    maven {
        url = uri("https://dl.bintray.com/michaelbull/maven")
    }
    maven {
        url = uri("https://gitlab.com/api/v4/groups/zsinz-kotlin/-/packages/maven")
        name = "GitLab"
    }
}

configurations.forEach { it.exclude("javax.annotation", "javax.annotation-api") }

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
        withJava()
    }
    js(IR) {
        useCommonJs()
        nodejs {}
        browser {
            binaries.executable()
            webpackTask {
                cssSupport.enabled = true
            }
            runTask {
                sourceMaps = true
                cssSupport.enabled = true
                devServer = devServer?.copy(
                    port = 8081
                )
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))

                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinSerializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.1.0")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")

                implementation(
                    project.dependencies.enforcedPlatform(
                        "$quarkusPlatformGroupId:$quarkusPlatformArtifactId:$quarkusPlatformVersion"
                    ))
                implementation("io.quarkus:quarkus-resteasy:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-resteasy-jackson:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-resteasy-mutiny:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-kotlin:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-smallrye-health:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-logging-json:$quarkusPlatformVersion")
                implementation("io.quarkus:quarkus-jackson:$quarkusPlatformVersion")
                implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$kotlinJacksonModuleVersion")
                implementation("com.michael-bull.kotlin-result:kotlin-result:$kotinResultVersion")

                implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:$kotlinxHtmlJvmVersion")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("io.quarkus:quarkus-junit5:$quarkusPlatformVersion")
                implementation("io.rest-assured:rest-assured:$restAssuredVersion")

                implementation("org.assertj:assertj-core:$assertjCoreVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$kotlinxCoroutinesVersion")
                implementation("uk.org.lidalia:slf4j-test:$slf4jTestVersion")

                implementation(kotlin("test"))

                implementation(kotlin("test-junit"))
            }
        }
        val jsMain by getting {
            dependencies {
                implementation(npm("core-js", "3.2.1"))
                implementation(kotlin("stdlib-js"))

                implementation("org.jetbrains:kotlin-extensions:$kotlinJsExtensionsVersion")
                implementation("org.jetbrains:kotlin-react:$kotlinReactVersion")

                implementation("org.jetbrains:kotlin-react-dom:$kotlinReactDomVersion")
                implementation("org.jetbrains:kotlin-redux:$kotlinReduxVersion")
                implementation("org.jetbrains:kotlin-react-redux:$kotlinReactReduxVersion")
                implementation("org.jetbrains:kotlin-react-router-dom:$kotlinReactRouterDomVersion")
                implementation("org.jetbrains:kotlin-css:$kotlinCssVersion")
                implementation("org.jetbrains:kotlin-styled:$kotlinCssVersion")

                implementation("com.zsinz:kotlin-react-mdwc:$kotlinReactMdwcVersion")

                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-js:$ktorVersion")
                implementation("io.ktor:ktor-client-json-js:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization-js:$ktorVersion")
                implementation("io.ktor:ktor-client-auth-basic-js:$ktorVersion")

                implementation(npm("css-loader", "3.4.2"))

                implementation(npm("react", "16.13.1"))
                implementation(npm("react-dom", "16.13.1"))
                implementation(npm("react-router-dom", "5.2.0"))
                implementation(npm("redux", "4.0.5"))
                implementation(npm("react-redux", "7.2.0"))

                implementation(npm("react-is", "16.13.1"))
                implementation(npm("inline-style-prefixer", "5.1.0"))
                implementation(npm("css-in-js-utils", "3.0.4"))
                implementation(npm("style-loader", "1.1.3"))
                implementation(npm("styled-components", "5.0.0"))
                implementation(npm("material-components-web", "3.2.0"))
                implementation(npm("rmwc", "5.7.2"))
            }
        }
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}

tasks {
    "test"(Test::class) {
        useJUnitPlatform()
    }
}

allOpen {
    annotation("javax.ws.rs.Path")
    annotation("javax.enterprise.context.ApplicationScoped")
    annotation("io.quarkus.test.junit.QuarkusTest")
}

quarkus {
    setSourceDir("$projectDir/src/jvmMain/kotlin")
    setOutputDirectory("$projectDir/build/classes/kotlin/jvm/main")
}

detekt {
    toolVersion = "1.10.0"
    buildUponDefaultConfig = true
}

tasks {
    quarkusDev {
        setSourceDir("$projectDir/src/jvmMain/kotlin")
    }
}

tasks.getByName<KotlinWebpack>("jsBrowserProductionWebpack") {
    destinationDirectory =
        File("$projectDir/build/classes/kotlin/jvm/main/META-INF/resources") // Allow the javascript to be accessible
    outputFileName = "kotlin-multiplatform-quarkus.js"
}

tasks.getByName<Jar>("jvmJar") {
    dependsOn(tasks.getByName("jsBrowserProductionWebpack"))
    val jsBrowserProductionWebpack = tasks.getByName<KotlinWebpack>("jsBrowserProductionWebpack")
    from(File(jsBrowserProductionWebpack.destinationDirectory, jsBrowserProductionWebpack.outputFileName))

    doLast {
        copy {
            from("$projectDir/src/jsMain/resources")
            into("$projectDir/build/classes/kotlin/jvm/main/META-INF/resources")
        }
    }
}
