import com.zsinz.kotlin.multiplatform.quarkus.client.application
import kotlinext.js.*
import kotlinx.browser.document
import react.dom.*

fun main(args: Array<String>) {
  render(document.getElementById("root")) {
    requireAll(kotlinext.js.require.context("@rmwc", true, js("/\\.css$/")))
    application()
  }
}
