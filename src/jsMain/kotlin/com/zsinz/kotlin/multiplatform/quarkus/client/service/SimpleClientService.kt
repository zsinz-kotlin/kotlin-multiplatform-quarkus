package com.zsinz.kotlin.multiplatform.quarkus.client.service

import common.domain.model.*
import io.ktor.client.request.get

object SimpleService : HttpService() {

    suspend fun getSimples(): List<SimpleModel> {
        return client.get<List<SimpleModel>>(getHostRequestUrl("/api/simples"))
    }

}