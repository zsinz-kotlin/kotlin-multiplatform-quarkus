package com.zsinz.kotlin.multiplatform.quarkus.client.service

import io.ktor.client.HttpClient
import io.ktor.client.engine.js.Js
import io.ktor.client.features.*
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.serialization.json.Json
import org.w3c.dom.Window

external val window: Window

open class HttpService {

    //@UnstableDefault
    val client = HttpClient(Js){
        // Configure default request feature.
        defaultRequest {
//            method = HttpMethod.Head
//            host = "127.0.0.1"
//            port = 8080
        }
    }.config {
        install(JsonFeature) {
            this.serializer = KotlinxSerializer(Json{
                isLenient = true
                ignoreUnknownKeys = true
            })
        }
    }


    fun getHostRequestUrl(endpoint: String): String {

      //  val port = ":"+window.location.port
        val localPort = window.location.port
        val port = if (localPort.isNotEmpty()) {
            if(localPort =="8081"){
                ":8080"
            }else {
                ":$localPort"
            }
        } else ""

        return "${window.location.protocol}//${window.location.hostname}${port}${endpoint}"
    }


}