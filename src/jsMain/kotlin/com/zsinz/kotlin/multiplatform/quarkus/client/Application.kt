package com.zsinz.kotlin.multiplatform.quarkus.client

import common.domain.model.SimpleModel
import kotlinext.js.js
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise
import kotlinx.css.LinearDimension
import kotlinx.css.height
import kotlinx.css.padding
import com.zsinz.kotlin.multiplatform.quarkus.client.service.SimpleService
import react.*
import react.dom.*
import com.zsinz.kotlin.react.mdwc.builders.*
import com.zsinz.kotlin.react.mdwc.builders.toggle
import com.zsinz.kotlin.react.mdwc.builders.inputField
import com.zsinz.kotlin.react.mdwc.types.ChipInfo
import com.zsinz.kotlin.react.mdwc.types.CustomEvent
import com.zsinz.kotlin.react.mdwc.types.SnackbarAction
import com.zsinz.kotlin.react.mdwc.types.TabBarState
import kotlinx.datetime.*
import styled.css
import styled.styledDiv

data class SampleControlsState(
    var selectOptions: Array<Any>,
    var selectedValue: String,
    var textField: String,
    var date: LocalDateTime?,

    var activeTabIndex: Number,
    var dialogOpen: Boolean,
    var SnackbarOpen: Boolean,
    var menuOpen: Boolean,

    var isCookieSelected: Boolean,
    var isPizzaSelected: Boolean,
    var isIcecreamSelected: Boolean,

    var toggled: Boolean,

    var simples: kotlin.collections.List<SimpleModel>
)

enum class TabPage(val label: String) { ONE("Tab One"), TWO("Tab Two") }

val applicationContainer = functionalComponent<RProps> { props ->
    val currentMoment: Instant = Clock.System.now()
    val datetimeInUtc: LocalDateTime = currentMoment.toLocalDateTime(TimeZone.UTC)

    var (sampleControlsState, setSampleControlsState) = useState(
        SampleControlsState(
            selectOptions = arrayOf(
                FormattedOptionData(label = "aaaa", value = "aaaa"),
                FormattedOptionData(label = "cccc", value = "cccc")
            ),
            selectedValue = "cccc",
            textField = "TextField Value",
            date = datetimeInUtc,

            activeTabIndex = 0,
            dialogOpen = false,
            SnackbarOpen = false,
            menuOpen = false,
            toggled = false,

            isCookieSelected = true,
            isPizzaSelected = false,
            isIcecreamSelected = false,



            simples = emptyList()
        )
    )

    useEffect(listOf()) {
        js("mdc.autoInit();")

        GlobalScope.promise(Dispatchers.Default, CoroutineStart.DEFAULT) {
            val list = SimpleService.getSimples()
            setSampleControlsState(sampleControlsState.copy(simples = list))
        }
    }

    fun separator() {
        div(classes = "separator") {}
    }

    fun header() {
        topAppBar {
            topAppBarRow {
                topAppBarSection(alignStart = true) {
                    topAppBarNavigationIcon {
                        attrs {
                            icon = "menu"
                        }
                    }
                    topAppBarTitle {
                        +"Welcome to React with Kotlin Demo - JVM & Native - Continue"
                    }
                }
                topAppBarSection(alignEnd = true) {
                    topAppBarActionItem {
                        attrs {
                            icon = "favorite"
                        }
                    }
                    topAppBarActionItem {
                        attrs {
                            icon = "star"
                        }
                    }
                    topAppBarActionItem {
                        attrs {
                            icon = "mood"
                        }
                    }
                }
            }
        }
        topAppBarFixedAdjust { }
    }

    fun inputControls() {
        grid() {

            gridCell(span = 2) {
                h3 {
                    +"Input Controls"
                }
            }
            gridCell(span = 8) {
                select(isRequired = false, label = "Filter Options") {
                    listItem(key = "Cookies") {
                        listItemGraphic(icon = "done")
                        +"Cookies"
                    }
                    listItem(key = "Pizza") {
                        listItemGraphic(icon = "done")
                        +"Pizza"
                    }
                }

                separator()

                div {
                    inputField(label = "Test Basic Text Input", value = sampleControlsState.textField) {
                        val text = it.currentTarget.value
                        console.log(it.toString() + "Content is : " + text)
                        setSampleControlsState(sampleControlsState.copy(textField = text))
                    }
                    div {
                        span(classes = "mdc-typography--caption") {
                            +"The Text output is : ${sampleControlsState.textField}"
                        }
                    }
                }

                separator()

                div {
                    inputField(
                        label = "Test Date Time Input",
                        value = sampleControlsState.date?.toString().orEmpty(),
                        inputType = "datetime-local"
                    ) {
                        val dateValue = it.currentTarget.value
                        console.log(it.toString() + "Content is : " + dateValue)
                    }
                }
                div {
                    span(classes = "mdc-typography--caption") {
                        +"The Text output is : ${sampleControlsState.date}"
                    }
                }
            }
        }
    }

    fun tabControls() {
        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Tab Controls"
                }
            }

            gridCell(span = 8) {
                tabBar(
                    activeTabIndex = sampleControlsState.activeTabIndex,
                    onActivate = {
                        setSampleControlsState(
                            sampleControlsState.copy(activeTabIndex = it.currentTarget.unsafeCast<TabBarState>().index)
                        )
                    }) {
                    TabPage.values().forEach {
                        tab(it.label)
                    }
                }
                div {
                    when (sampleControlsState.activeTabIndex) {
                        TabPage.ONE.ordinal -> div() {
                            h4 { +"Tab One Content" }
                        }
                        TabPage.TWO.ordinal -> div() {
                            h4 { +"Tab Two Content" }
                        }
                    }
                }
            }
        }
    }

    fun dialogControls() {
        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Dialog Controls"
                }
            }
            gridCell(span = 8) {
                button("Show Me The Dialog", raised = true) {
                    setSampleControlsState(sampleControlsState.copy(dialogOpen = true))
                }
                dialog(
                    open = sampleControlsState.dialogOpen,
                    onClose = { setSampleControlsState(sampleControlsState.copy(dialogOpen = false)) }) {
                    dialogTitle { +"SomeTitle" }
                    dialogContent {
                        div {
                            h4 { +"Here is the content" }
                        }
                    }
                    dialogActions { dialogButton("Close", "close") }
                }
            }
        }
    }

    fun snackbarControls() {
        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Snackbar Controls"
                }
            }
            gridCell(span = 8) {
                button("Show Me The Snackbar", raised = true) {
                    setSampleControlsState(sampleControlsState.copy(SnackbarOpen = true))
                }
                div {
                    val myAction: () -> ReactElement = {
                        console.log("Creating the Snackbar Action")
                        SnackbarAction {
                            attrs.label = "TEST"
                        }
                    }
                    snackbar(
                        open = sampleControlsState.SnackbarOpen,
                        onClose = { setSampleControlsState(sampleControlsState.copy(SnackbarOpen = false)) },
                        dismissesOnAction = true,
                        message = "Error: This is an example of an error message being show",
                        action = myAction
                    )
                }
            }
        }
    }

    fun toggleControls() {
        grid {
            gridCell(span = 2) {
                h3 {
                    +"Toggle Controls"
                }
            }
            gridCell(span = 8) {
                toggle(label = "A Toggle", checked = sampleControlsState.toggled) {
                    var value = it.currentTarget.asDynamic().checked.unsafeCast<Boolean>()
                    setSampleControlsState(sampleControlsState.copy(toggled = value))
                }
                dialog(
                    open = sampleControlsState.dialogOpen,
                    onClose = { setSampleControlsState(sampleControlsState.copy(dialogOpen = false)) }) {
                    dialogTitle { +"SomeTitle" }
                    dialogContent {
                        div {
                            h4 { +"Here is the content" }
                        }
                    }
                    dialogActions { dialogButton("Close", "close") }
                }
            }
        }
    }

    fun listControls() {
        grid {
            gridCell(span = 2) {
                h3 {
                    +"List Controls"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                gridInner {
                    gridCell(classes = "table-wrapper", span = 12) {
                        h4 {
                            +"Static - List Controls"
                        }
                        list() {
                            listItem("1") {
                                +"Item 1"
                            }
                            listItem("2", selected = true) {
                                +"Item 2"
                            }
                        }
                    }
                    separator()
                    // Dynamic - from api call
                    gridCell(classes = "table-wrapper", span = 12) {
                        h4 {
                            +"Dynamic From Server - List Controls"
                        }
                        list() {
                            for (index in sampleControlsState.simples.indices) {
                                listItem("" + index) {
                                    +sampleControlsState.simples[index].content
                                }
                            }
                        }
                    }
                    separator()

                    gridCell(classes = "table-wrapper", span = 12) {
                        list() {
                            listItem("star") {

                                listItemGraphic(icon = "star_border")
                                listItemText {
                                    listItemPrimaryText {
                                        +"Cookies"
                                    }
                                    listItemSecondaryText {
                                        +"\$4.99 a dozen"
                                    }
                                }
                                listItemMeta(icon = "info") {}
                            }
                            listItem("pizza") {

                                listItemGraphic(icon = "local_pizza")
                                listItemText {
                                    listItemPrimaryText {
                                        +"Pizza"
                                    }
                                    listItemSecondaryText {
                                        +"\$1.99 a slice"
                                    }
                                }
                                listItemMeta(icon = "info") {}
                            }
                            listItem("mood", activated = true) {

                                listItemGraphic(icon = "mood")
                                listItemText {
                                    listItemPrimaryText {
                                        +"Icecream"
                                    }
                                    listItemSecondaryText {
                                        +"\$0.99 a scoop"
                                    }
                                }
                                listItemMeta(icon = "info") {
                                    +"Winner!"
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun menuControls() {
        grid {
            gridCell(span = 2) {
                h3 {
                    +"Menu Controls"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                gridInner {
                    gridCell(classes = "table-wrapper", span = 12) {
                        menuSurfaceAnchor {
                            menu(
                                open = sampleControlsState.menuOpen,
                                onClose = { evt -> setSampleControlsState(
                                    sampleControlsState.copy(menuOpen = false)
                                ) }) {
                                menuItem { +"Cookies" }
                                menuItem { +"Pizza" }
                                menuItem { +"Icecream" }
                            }
                            button(text = "Menu", raised = true) {
                                setSampleControlsState(
                                    sampleControlsState.copy(
                                        menuOpen = !sampleControlsState.menuOpen
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    fun dataTable() {

        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Data Table Controls"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                datatable {
                    datatableContent {
                        datatableHead {
                            datatableRow(key = "HeaderRow") {
                                datatableHeadCell(className = "tableColumn") {
                                    +"Environment Name"
                                }
                                datatableHeadCell(className = "tableColumn", alignEnd = true) {
                                    +"Actions"
                                }
                            }
                        }
                        datatableBody {
                            datatableRow(key = "row") {
                                datatableCell {
                                    +"Hello"
                                }

                                datatableCell(alignEnd = true) {
                                    editActionButton {
                                        it.preventDefault()
                                    }
                                    deleteActionButton {
                                        it.preventDefault()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun chipControls() {
        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Chips"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                gridInner {
                    gridCell(classes = "table-wrapper", span = 12) {
                        chipSet {
                            chip(selected = true, label = "Cookies")
                            chip(label = "Pizza")
                            chip(label = "Icecream")
                        }
                        separator()
                        chipSet(choice = true) {
                            chip(
                                selected = sampleControlsState.isCookieSelected,
                                label = "Cookies",
                                onInteraction = { evt: CustomEvent<ChipInfo> ->
                                    setSampleControlsState(
                                        sampleControlsState.copy(
                                            isCookieSelected = !sampleControlsState.isCookieSelected
                                        )
                                    )
                                })
                            chip(
                                selected = sampleControlsState.isPizzaSelected,
                                label = "Pizza",
                                onInteraction = { evt: CustomEvent<ChipInfo> ->
                                    setSampleControlsState(
                                        sampleControlsState.copy(
                                            isPizzaSelected = !sampleControlsState.isPizzaSelected
                                        )
                                    )
                                })
                            chip(
                                selected = sampleControlsState.isIcecreamSelected,
                                label = "Icecream",
                                onInteraction = { evt: CustomEvent<ChipInfo> ->
                                    setSampleControlsState(
                                        sampleControlsState.copy(
                                            isIcecreamSelected = !sampleControlsState.isIcecreamSelected
                                        )
                                    )
                                })
                        }
                    }
                }
            }
        }
    }

    fun cardControls() {
        grid() {
            gridCell(span = 2) {
                h3 {
                    +"Cards"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                gridInner {
                    gridCell(span = 6) {
                        card() {
                            cardPrimaryAction {
                                cardMedia(
                                    isSixteenByNine = true,
                                    style = js { backgroundImage = "url(images/mb-bg-fb-16.png)" })

                                div() {
                                    typography(use = "headline6", tag = "h2") {
                                        +"Our Changing Planet"
                                    }
                                    typography(
                                        use = "subtitle2",
                                        tag = "h3",
                                        theme = "textSecondaryOnBackground",
                                        style = js { marginTop = "-1rem" }
                                    ) {
                                        +"Our Changing Planet"
                                    }
                                    typography(use = "body1", tag = "div", theme = "textSecondaryOnBackground") {
                                        +"Visit ten places on our planet"
                                    }
                                }
                            }
                            cardActions {
                                cardActionButtons {
                                    cardActionButton(text = "Read") {}
                                    cardActionButton(text = "Bookmark") {}
                                }
                                cardActionIcons {
                                    cardActionIcon(onIcon = "favorite", icon = "favorite_border") {}
                                    cardActionIcon(icon = "share") {}
                                    cardActionIcon(icon = "more_vert") {}
                                }
                            }
                        }
                        separator()
                    }
                }
            }
        }
    }

    fun drawerControl() {
        grid() {
            gridCell(span = 2) {
                h3 {
//                    attrs {
//                        jsStyle { }
//                    }
                    +"Drawer"
                }
            }
            gridCell(classes = "table-wrapper", span = 8) {
                gridInner {
                    gridCell(span = 6) {
                        drawer() {
                            drawerHeader {
                                drawerTitle {
                                    +"DrawerHeader"
                                }
                                drawerSubTitle {
                                    +"Subtitle"
                                }
                            }
                            drawerContent {
                                list {
                                    listItem("Cookies") {
                                        +"Cookies"
                                    }
                                    listItem("Pizza") {
                                        +"Pizza"
                                    }
                                    listItem("Icecream") {
                                        +"Icecream"
                                    }
                                }
                            }
                        }
                        separator()
                    }
                }
            }
        }
    }

    fun render() {
        header()

        styledDiv() {
            css {
                this.height = LinearDimension("100rem")
                this.padding(all = LinearDimension("1rem"))
            }

            inputControls()

            tabControls()

            snackbarControls()

            dialogControls()

            toggleControls()

            listControls()

            menuControls()

            dataTable()

            chipControls()

            cardControls()

            drawerControl()
        }
    }

    render()
}

fun RBuilder.application() = child(applicationContainer) {
}
