package com.nanthealth.admission.commands

import com.nanthealth.admission.pipeline.Handler
import com.nanthealth.admission.pipeline.ICommand
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class CommandHandler<V>(val name: String = "") : Handler<ICommand, V, ApiError>{
    val logger: Logger = LoggerFactory.getLogger(javaClass)
}


inline fun <R, V> CommandHandler<V>.trace(block: () -> R): R {
    logger.trace("BEGIN: $name processing")
    try {
        return block()
    } finally {
        logger.trace("END: $name processing")
    }
}
