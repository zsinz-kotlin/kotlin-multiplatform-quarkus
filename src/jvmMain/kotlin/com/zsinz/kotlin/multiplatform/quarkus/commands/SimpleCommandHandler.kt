package com.nanthealth.admission.commands

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import com.nanthealth.admission.pipeline.ICommand
import common.domain.model.SimpleModel

class SimpleCommand() : ICommand

class SimpleCommandHandler :
    CommandHandler<List<SimpleModel>>("Generate") {

    override suspend fun execute(request: ICommand): Result<List<SimpleModel>, ApiError> = trace {
        require(request is SimpleCommand) {
            "The request type must be a Test Command"
        }
        try {
            val simpleCommand = request as SimpleCommand
            return Ok(listOf(SimpleModel("From Server: Model 1"), SimpleModel("From Server: Model 2")))
        }catch(exception: Exception) {
            return Err(ApiError("Dunno"))
        }
    }
}







