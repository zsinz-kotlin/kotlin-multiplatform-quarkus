package com.nanthealth.admission.commands

open class ApiError constructor(open val reason: String)
