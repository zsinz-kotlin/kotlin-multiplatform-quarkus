package com.zsinz.kotlin.multiplatform.quarkus

import com.github.michaelbull.result.mapBoth
import com.nanthealth.admission.commands.SimpleCommand
import com.nanthealth.admission.commands.SimpleCommandHandler
import io.smallrye.mutiny.Uni
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.CompletableFuture
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/api")
class SimpleApiController {
    val logger: Logger = LoggerFactory.getLogger(javaClass)
    val simpleResponseHandler = SimpleCommandHandler()

    @GET
    @Path("/simples")
    @Produces(MediaType.APPLICATION_JSON)
    fun simples(): Uni<Response> {
        return Uni.createFrom().completionStage(CompletableFuture.supplyAsync {
            val result = runBlocking {
                try {
                    simpleResponseHandler.execute(SimpleCommand())
                        .mapBoth(
                            { output ->
                                logger.info("New Info")
                                Response.ok(output).build()
                            },
                            { error ->
                                logger.error("Test Log: Cannot process this request : ${error.reason}")
                                Response.status(Response.Status.INTERNAL_SERVER_ERROR.statusCode, error.reason).build();
                            })
                } catch (e: Exception) {
                    logger.error(e.localizedMessage)
                    Response.status(
                        Response.Status.INTERNAL_SERVER_ERROR.statusCode,
                        """{"error":"${e.localizedMessage}"}"""
                    )
                        .build()
                }
            }
            result
        })
    }
}
