package com.zsinz.kotlin.multiplatform.quarkus.cdi

object ServiceLocatorProperties {
    /**
     * The Kubernetes Cluster URL to connect
     */
    const val K8S_SVC_CLUSTER_URL = "K8S_SVC_CLUSTER_URL"
    /**
     * User used to authenticate to the Kubernetes Cluster
     */
    const val K8S_SVC_CLUSTER_USER = "K8S_SVC_CLUSTER_USER"
    /**
     * User password used to authenticate to the Kubernetes Cluster
     */
    const val K8S_SVC_CLUSTER_PWD = "K8S_SVC_CLUSTER_PWD"
    /**
     * Should the k8s client validate the SSL certificate? Set to false when using self signed certificates
     */
    const val K8S_SVC_CLUSTER_VALIDATE_CERT = "K8S_SVC_CLUSTER_VALIDATE_CERT"
    /**
     * Used instead of user and password authentication
     */
    const val K8S_SVC_CLUSTER_TOKEN = "K8S_SVC_CLUSTER_TOKEN"
}