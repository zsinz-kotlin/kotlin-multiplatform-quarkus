package com.nanthealth.admission.pipeline

import com.github.michaelbull.result.Result

interface ICommand

interface Handler<T, R, E> {
    suspend fun execute(request: T): Result<R, E>
}

