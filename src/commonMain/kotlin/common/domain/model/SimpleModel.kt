package common.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class SimpleModel(val content:String)